"""
    Problem 1
"""
def matrix_solve(board,string,i,j,w):
    while((i!=len(board)) and (j!=len(board[0]))):
          if(w==len(string)):
              return True
          if(board[i][j+1]==string[w] or board[i+1][j]==string[w] or board[i][j-1]==string[w] or board[i-1][j]==string[w]):
              return (matrix_solve(board,string,i+1,j,w+1) or matrix_solve(board,string,i-1,j,w+1) or matrix_solve(board,string,i,j+1,w+1) or matrix_solve(board,string,i,j-1,w+1))
          else:
              return False
    return False


board=input()
string=input()
print(matrix_solve(board,string,0,0,0))

"""
    Problem 2
"""

def matched(str):
    ope = []
    clo = []
    for i in range(0,len(str)):
        l = str[i]
        if l == "(":
            ope = ope + ["("]
        elif l=="{":
            ope=ope+["{"]
        elif l=="[":
            ope=ope+["["]
            
        else:
            if l == ")":
                clo = clo  + [")"]
            elif l == "}":
                clo = clo  + ["}"]
            elif l == "]":
                clo = clo  + ["]"]
            else:
                return(ope, clo)
        
    if len(ope)==len(clo):
        return True
    else:
        return False

str=input()
print(matched(str))
